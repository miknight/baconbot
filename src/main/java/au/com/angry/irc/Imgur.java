package au.com.angry.irc;

import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Imgur implements MessageListener
{

    private static final Pattern[] PATTERNS =  new Pattern[] {
            Pattern.compile(".*imgur.com.*/gallery/([a-zA-Z0-9]+).*")
    };

    private final HttpClient client = new HttpClient();
    private final JSONParser parser = new JSONParser();

    public String getImageDescription(String imageKey) throws IOException
    {
        HttpMethod get = new GetMethod("https://api.imgur.com/3/image/"+imageKey);
        get.setRequestHeader("Authorization","Client-ID 9d7515757d4f56d");
        int status = client.executeMethod(get);
        if(status==200)
        {
            try
            {
                JSONObject object = (JSONObject) parser.parse(get.getResponseBodyAsString());
                JSONObject data = (JSONObject)object.get("data");
                String title = data.get("title").toString();
                String description = data.get("description").toString();
                return title+"\n"+description;
            }
            catch (ParseException e)
            {
                throw new IOException(e);
            }
        }
        return "";
    }

    private String getImageId(String message) {
        for (Pattern pattern : PATTERNS) {
            Matcher matcher = pattern.matcher(message);
            if (matcher.matches())
                return matcher.group(1);
        }
        return null;
    }

    private String getTarget(String channel, String sender) {
        return StringUtils.isNotBlank(channel) ? channel : sender;
    }

    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String sender = event.getSender();
        String message = event.getMessage();
        String channel = event.getChannel();

        if (bot.getNick().equalsIgnoreCase(sender))
            return;

        try {
            String imageId = getImageId(message);
            if (imageId != null) {
                try
                {
                    String data = getImageDescription(imageId);
                    String[] lines = StringUtils.split(data, "\n");
                    if(lines.length==1) {
                        bot.sendMessage(getTarget(channel, sender), lines[0]);
                    } else {
                        for (String line : lines)
                            bot.sendMessage(getTarget(channel, sender), "| " + line);
                    }
                }
                catch (IOException e)
                {
                    // ignore
                }
                event.consume();
            }
        } catch (IllegalStateException e) {
            bot.sendMessage(getTarget(channel, sender), e);
        }
    }
}
