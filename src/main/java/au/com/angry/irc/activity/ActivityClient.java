package au.com.angry.irc.activity;

import au.com.angry.irc.ConnectionSettings;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 */
public class ActivityClient
{

    public static void main(String[] args)
    {
        ConnectionSettings settings = new ConnectionSettings();
        settings.setDebug(true);
        settings.setUpdatesUrl("http://wiki.angry.com.au/rest/wow/1.0/wow/activity.xml");

        ActivityClient c = new ActivityClient(settings);
        List<Activity> list = c.getUpdates(new Date(0));
        for (Activity activity : list)
            System.out.println(activity.toString());
    }

    public ActivityClient(ConnectionSettings settings)
    {
        this.url = settings.getUpdatesUrl();
        this.debug = settings.isDebug();
    }

    private final String url;
    private final boolean debug;

    public List<Activity> getUpdates(Date date)
    {
        List<Activity> updates = new LinkedList<Activity>();
        try
        {
            String urlString = this.url;
            if (date != null)
                urlString = urlString + "?time=" + date.getTime();
            if(debug)
                System.out.println("Updates from [ " + urlString + " ]");
            URL url = new URL(urlString);
            InputStream in = url.openStream();

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = docFactory.newDocumentBuilder();
            Document document = builder.parse(in);

            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();

            Node list = (Node) xpath.evaluate("//activity-list", document, XPathConstants.NODE);
            NodeList childNodes = list.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++)
            {
                Node node = childNodes.item(i);
                String title = node.getAttributes().getNamedItem("title").getTextContent();
                String type = node.getAttributes().getNamedItem("type").getTextContent();
                String username = node.getAttributes().getNamedItem("username").getTextContent();
                String update = node.getAttributes().getNamedItem("update").getTextContent();
                String urlPath = node.getAttributes().getNamedItem("url").getTextContent();
                long time = Long.parseLong(node.getAttributes().getNamedItem("time").getTextContent());
                updates.add(new Activity(username, type, title, update, urlPath, time));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return updates;
    }
}