package au.com.angry.irc.activity;

import org.apache.commons.lang.StringUtils;

public class Activity
{
    private final String username;
    private final String type;
    private final String title;
    private final String update;
    private String url;
    private long time;

    public Activity(String username, String type, String title, String update, String url, long time)
    {
        this.username = username;
        this.type = type;
        this.title = title;
        this.update = update;
        this.url = url;
        this.time = time;
    }

    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(StringUtils.isBlank(username) ? "anonymous" : username);
        builder.append(" ");
        builder.append(update);
        builder.append(" ");
        builder.append(type);
        builder.append(" '");
        builder.append(title);
        builder.append("' [ ");
        builder.append(url);
        builder.append(" ]");
        return builder.toString();
    }

    public long getTime()
    {
        return time;
    }
}
