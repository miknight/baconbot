package au.com.angry.irc.activity;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.IrcJob;
import org.quartz.*;

import java.util.Date;
import java.util.List;

/**
 */
public class ActivityJob extends IrcJob
{
    protected void execute(HoggerBot bot, Date prev)
    {
        Date lastUpdateDate = bot.getLastUpdatesDate();
        ActivityClient client = new ActivityClient(bot.getSettings());

        if (lastUpdateDate == null)
        {
            // get all updates
            List<Activity> list = client.getUpdates(new Date(0));
            long lastUpdateTime = 0;
            for (Activity activity : list)
                lastUpdateTime = (activity.getTime() > lastUpdateTime) ? activity.getTime() : lastUpdateTime;
            if (lastUpdateTime > 0)
            {
                System.out.println("First run - set last Update Time to [ " + lastUpdateTime + " ]");
                bot.setLastUpdatesDate(new Date(lastUpdateTime));
            }
        }
        else
        {
            List<Activity> list = client.getUpdates(new Date(lastUpdateDate.getTime() + 1));
            if (list.size() > 0 || bot.getSettings().isDebug())
                System.out.println("Found [ " + list.size() + " ] updates since [ " + lastUpdateDate.getTime() + " ]");
            long lastUpdateTime = 0;
            for (Activity activity : list)
            {
                lastUpdateTime = (activity.getTime() > lastUpdateTime) ? activity.getTime() : lastUpdateTime;
                bot.doSend(activity.toString());
            }
            if (lastUpdateTime > 0)
                bot.setLastUpdatesDate(new Date(lastUpdateTime));
        }
    }

    public static void register(Scheduler sched, JobDataMap data) throws SchedulerException
    {
        JobDetail updatesJob = new JobDetail("updatesJob", null, ActivityJob.class);
        updatesJob.setJobDataMap(data);
        Trigger updatesTrigger = TriggerUtils.makeMinutelyTrigger(1);
        updatesTrigger.setName("updatesTrigger");
        sched.scheduleJob(updatesJob, updatesTrigger);
    }
}