package au.com.angry.irc;

import java.util.Random;

/**
 * A totally made-up algorithm to reduce bot spam from random replies.
 *
 * * Each successfully sent message halves the chance of the bot sending subsequent messages.
 * * If the bot is quiet for a whole minute, half of the message count is forgotten
 * * If the bot is quiet for a whole five minutes, the counter is reset to zero
 */
public class BackoffCounter
{
    private Random random = new Random();
    long lastMessageSent = System.currentTimeMillis();
    int bullshitCounter = 0;

    public synchronized boolean shouldSend()
    {
        updateBullshitCounter();

        double chanceOfMessage = 1.0 / Math.pow(2, bullshitCounter);

        if (random.nextDouble() < chanceOfMessage)
        {
            bullshitCounter++;
            lastMessageSent = System.currentTimeMillis();
            return true;
        }

        return false;
    }

    private void updateBullshitCounter()
    {
        long elapsedMinutes = (System.currentTimeMillis() - lastMessageSent) / 60000;

        if (elapsedMinutes >= 1)
            bullshitCounter = bullshitCounter / 2;

        if (elapsedMinutes >= 5)
            bullshitCounter = 0;
    }
}
