package au.com.angry.irc;

import java.util.Random;

/**
 * Can be used to as a random check with increased probability the more times you
 * ask if its true.
 */
public class Chance {

    private final Random random = new Random();
    private final double chance;
    private double realChance;
    private final double growth;

    public Chance(double chance, double growth) {
        this.chance = chance;
        this.realChance = chance;
        this.growth = growth;
    }

    public Chance(double chance) {
        this(chance,11d/10d);
    }

    public boolean isTrue() {
        boolean match = false;
        double v = random.nextDouble();
        if ((this.chance == 0 || v < this.realChance)) {
            match = true;
            this.realChance = this.chance;
        } else {
            if (this.realChance > 1.0d)
                this.realChance = 1.0d;
            this.realChance = this.realChance * growth;
        }
        return match;
    }

    public void reset()
    {
        this.realChance = this.chance;
    }
}
