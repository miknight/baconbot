package au.com.angry.irc.game;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;

/**
 */
public class TwentyQuestionsListener implements MessageListener
{
    private static final String trigger = "are you thinking of";

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        // Only one game of any type going at a time.
        if(!bot.contains("game")) {
            if ((event.getMessage() != null) && event.getMessage().toLowerCase().contains(trigger))
            {
                TwentyQuestionsGame twentyQuestionsGame = new TwentyQuestionsGame();
                twentyQuestionsGame.start (bot, new String [] {});
                bot.set("game", twentyQuestionsGame);
            }
        }
    }
}
