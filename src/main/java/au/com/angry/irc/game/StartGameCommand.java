package au.com.angry.irc.game;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;

public class StartGameCommand implements Command
{

    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        if(!bot.contains("game") && args.length==0) {
            NumberGame numberGame = new NumberGame();
            numberGame.start(bot,args);
            bot.set("game", numberGame);
            return true;
        }
        return false;
    }
}
