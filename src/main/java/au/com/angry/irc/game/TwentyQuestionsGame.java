package au.com.angry.irc.game;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import org.apache.commons.lang.math.RandomUtils;

/**
 * A highly advanced guessing game.
 */
public class TwentyQuestionsGame implements Game
{
    private final int maxQuestions = 10;
    private int remainingQuestions;

    public TwentyQuestionsGame() {}

    public void start(HoggerBot bot, String[] args)
    {
        remainingQuestions = maxQuestions;
        bot.doSend("Yes, I am! Find out more about what I'm thinking of by asking questions starting with 'ask <question>'");
        bot.doSend("When you're ready to answer, say 'guess <answer>'");
    }
    
    public boolean ask(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        // Questions are randomly right or wrong throughout the game.
        boolean result = (RandomUtils.nextBoolean());
        handleQuestion (bot, event, result, false);
        return true;
    }
    
    public boolean guess(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        // Guesses towards the end of the game are more likely to be correct.
        boolean result = false;
        if (remainingQuestions == 1) result = true;
        if (remainingQuestions > 1) result = (RandomUtils.nextInt(remainingQuestions/2) == 0);
        handleQuestion (bot, event, result, true);
        return true;
    }
    
    public void handleQuestion(HoggerBot bot, MessageEvent event, boolean result, boolean isGuess)
    {
        remainingQuestions--;
        if (remainingQuestions < 0)
        {
            bot.clear("game");
            return;
        }
        
        if (isGuess && result)
        {
            bot.reply(event, "Yes! You win!");
            bot.clear("game");
            return;
        }
        
        if (remainingQuestions == 0)
        {
            bot.reply(event, (result ? "Yes" : "No") + ". You're out of questions. Game over.");
            bot.clear("game");
            return;
        }
        
        if (remainingQuestions == 1)
        {
            bot.reply(event, (result ? "Yes" : "No") + ". You have only 1 question left. Better guess something!");
        }
        else
        {
            bot.reply(event, (result ? "Yes" : "No") + ". You have " + remainingQuestions + " questions left");
        }
    }
}
