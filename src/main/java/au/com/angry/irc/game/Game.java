package au.com.angry.irc.game;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;

/**
 * A Guessing game of some sort
 */
public interface Game
{
    void start(HoggerBot bot, String[] args);

    boolean guess(HoggerBot bot, MessageEvent event, String command, String[] args);

    boolean ask(HoggerBot bot, MessageEvent event, String command, String[] args);
}
