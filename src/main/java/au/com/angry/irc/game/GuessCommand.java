package au.com.angry.irc.game;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;

/**
 */
public class GuessCommand implements Command
{
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        return bot.contains("game") && ((Game) bot.get("game")).guess(bot, event, command, args);
    }
}
