package au.com.angry.irc.game;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import org.apache.commons.lang.math.RandomUtils;

/**
 * A simple number guessing game.
 */
public class NumberGame implements Game
{
    private final int answer;

    public NumberGame()
    {
        answer = RandomUtils.nextInt(100)+1;
    }

    public void start(HoggerBot bot, String[] args)
    {
        bot.doSend("starting a number guessing game!");
        bot.doSend("guess a number between 1 and 100 inclusive, use: guess <answer>");
    }

    public boolean guess(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        try
        {
            int guess = Integer.parseInt(args[0]);
            if(guess>=1&&guess<=100) {
                if(guess==answer) {
                    bot.reply(event,"you got it! you win!");
                    bot.clear("game");
                } else {
                    if(guess<answer) {
                        bot.reply(event,"guess higher!");
                    } else {
                        bot.reply(event,"guess lower!");
                    }
                }
                return true;
            }
        }
        catch (NumberFormatException e)
        {
            // ignore
        }
        return false;
    }
    
    public boolean ask(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        return false;
    }

}
