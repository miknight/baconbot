package au.com.angry.irc.lists;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mjensen on 24/02/15.
 */
public class ListsCommand implements Command
{
    public static final String ALL_LIST = "_all";
    private final PersonListManager personListManager;

    public ListsCommand(PersonListManager personListManager)
    {
        this.personListManager = personListManager;
    }

    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        if (args.length == 0)
        {
            List<PersonList> allLists = personListManager.getAllLists();
            if (allLists.isEmpty())
            {
                bot.reply(event, "no notification lists defined");
            }
            else
            {
                StringBuilder buf = new StringBuilder();
                for (PersonList pl : allLists)
                {
                    if (buf.length() > 0) buf.append(", ");
                    buf.append(pl.getName());
                    if (pl.getPersons().isEmpty())
                    {
                        buf.append(" (empty)");
                    }
                    else
                    {
                        buf.append(" (");
                        buf.append(pl.getPersons().stream().map(s -> "*").collect(Collectors.joining()));
                        buf.append(")");
                    }
                }
                bot.reply(event, buf.toString());
            }
        }
        else if (args.length > 1 && args[0].toLowerCase().equals("add"))
        {
            String listName = args[1];
            if (!listName.equals(ALL_LIST))
            {
                bot.reply(event, "you have created a new list '" + listName + "'");
                personListManager.addPersonList(listName);
            }
            else
            {
                return false;
            }
        }
        else if (args.length > 1 && args[0].toLowerCase().equals("delete"))
        {
            String listName = args[1];
            if (listName.equals(ALL_LIST))
            {
                if (personListManager.hasList(ALL_LIST))
                {
                    bot.reply(event, "you have deleted a list '_all'");
                    personListManager.deletePersonList(ALL_LIST);
                }
                else
                {
                    for (PersonList personList : personListManager.getAllLists())
                    {
                        bot.reply(event, "you have deleted a list '" + personList.getName() + "'");
                        personListManager.deletePersonList(personList.getName());
                    }
                }
            }
            else
            {
                bot.reply(event, "you have deleted a list '" + listName + "'");
                personListManager.deletePersonList(listName);
            }
        }
        else if (args.length > 1 && args[0].toLowerCase().equals("join"))
        {
            String listName = args[1];
            if (!listName.equals(ALL_LIST))
            {
                bot.reply(event, "you have joined the list '" + listName + "'");
                personListManager.addPersonToList(listName, event.getSender());
            }
            else
            {
                return false;
            }
        }
        else if (args.length > 1 && args[0].toLowerCase().equals("leave"))
        {
            String listName = args[1];
            if (!listName.equals(ALL_LIST))
            {
                bot.reply(event, "you have left the list '" + listName + "'");
                personListManager.removePersonFromList(listName, event.getSender());
            }
            else
            {
                return false;
            }
        }
        else if (args.length > 1 && args[0].toLowerCase().equals("members"))
        {
            String listName = args[1];
            if (!listName.equals(ALL_LIST))
            {

                PersonList personList = personListManager.getPersonList(listName);
                StringBuilder buf = new StringBuilder();
                if (!personList.getPersons().isEmpty())
                {
                    for (String person : personList.getPersons())
                    {
                        if (buf.length() > 0)
                        {
                            buf.append(", ");
                        }
                        buf.append(person);
                    }
                }
                else
                {
                    bot.reply(event, "this list is empty");
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            bot.reply(event, "lists add|delete|join|leave <list name>");
        }

        return true;
    }


}
