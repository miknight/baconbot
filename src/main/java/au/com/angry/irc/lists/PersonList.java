package au.com.angry.irc.lists;

import com.google.common.collect.Lists;
import java.util.List;

/**
 * Created by mjensen on 24/02/15.
 */
public class PersonList
{
    private final String name;
    private List<String> persons;

    public PersonList(String name, Iterable<String> persons)
    {
        this.name = name;
        this.persons = Lists.newArrayList(persons);
    }

    public String getName()
    {
        return name;
    }

    public List<String> getPersons()
    {
        return persons;
    }
}
