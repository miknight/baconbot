package au.com.angry.irc.lists;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListsListener implements MessageListener
{
    private final PersonListManager listManager;

    public ListsListener(PersonListManager listManager)
    {
        this.listManager = listManager;
    }

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String sender = event.getSender();
        String message = event.getMessage();
        String channel = event.getChannel();

//        if (bot.getNick().equalsIgnoreCase(sender))
//            return;

        List<PersonList> lists = listManager.getAllLists();

        Set<String> people = new HashSet<>();

        lists.stream().filter(list -> message.toLowerCase().contains("@" + list.getName().toLowerCase())).forEach(list -> {
            people.addAll(list.getPersons());
        });

        StringBuilder buf = new StringBuilder();
        for (String person : people)
        {
            if (!bot.getNick().equalsIgnoreCase(person) && !sender.equals(person))
            {
                if (buf.length() > 0)
                    buf.append(", ");
                buf.append(person);
            }
        }

        if (buf.length() > 0)
            bot.sendMessage(channel, "cc " + buf.toString());
    }

}
