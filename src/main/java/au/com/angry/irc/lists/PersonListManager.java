package au.com.angry.irc.lists;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class PersonListManager
{
    private final DBCollection coll;

    public PersonListManager(DBCollection coll)
    {
        this.coll = coll;
    }

    public boolean hasList(String name) {
        for (PersonList personList : getAllLists())
        {
            if(personList.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    public PersonList getPersonList(String name)
    {
        DBCursor cur = findByName(name);

        if (cur.hasNext())
        {
            DBObject obj = cur.next();
            return getPersonList(obj);
        }
        else
        {
            return new PersonList(name, Collections.<String>emptyList());
        }
    }

    private PersonList getPersonList(DBObject obj)
    {
        //noinspection unchecked
        return new PersonList((String) obj.get("name"), (List<String>) obj.get("persons"));
    }

    public PersonList addPersonList(String name)
    {
        DBObject obj = new BasicDBObject();
        obj.put("name", name);
        obj.put("lcname", name.toLowerCase(Locale.ENGLISH));
        obj.put("persons", Collections.emptyList());
        coll.save(obj);
        return getPersonList(obj);
    }

    private DBCursor findByName(String name)
    {
        DBObject query = new BasicDBObject();
        query.put("lcname", name.toLowerCase(Locale.ENGLISH));
        return coll.find(query);
    }

    public List<PersonList> getAllLists()
    {
        DBObject query = new BasicDBObject();
        return coll.find(query).toArray().stream().map(this::getPersonList).collect(Collectors.toList());
    }

    public void addPersonToList(String listName, String person)
    {
        DBCursor cur = findByName(listName);

        if(!cur.hasNext()) {
            addPersonList(listName);
            cur = findByName(listName);
        }

        DBObject obj = cur.next();
        //noinspection unchecked
        List<String> persons = (List<String>) obj.get("persons");
        if(!persons.contains(person))
            persons.add(person);
        obj.put("persons",persons);
        coll.save(obj);
    }

    public void removePersonFromList(String listName, String person)
    {
        DBCursor cur = findByName(listName);
        DBObject obj = cur.next();
        //noinspection unchecked
        List<String> persons = (List<String>) obj.get("persons");
        while(persons.contains(person))
            persons.remove(person);
        obj.put("persons",persons);
        coll.save(obj);
    }

    public void deletePersonList(String listName)
    {
        DBObject obj = findByName(listName).next();
        coll.remove(obj);
    }

}
