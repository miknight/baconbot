package au.com.angry.irc;

public class ProbabilityConverter implements Converter<Double>
{
    public boolean isValid(String s)
    {
        try
        {
            Double d = Double.valueOf(s);
            return d != null && d >= 0.05 && d <= 1.0;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }
    public String getConstraint()
    {
        return "must be a floating point number between 0.05 and 1.0";
    }

    public Double convert(String s) throws IllegalArgumentException
    {
        return Math.round(Double.valueOf(s) * 100) / 100.0;
    }
}
