package au.com.angry.irc.rhyme;

import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 */
public class Rhymes
{
    private final Random random = new Random();
    private List<Rhyme> rhymes = new ArrayList<Rhyme>();

    public Rhymes(String filename)
    {
        try
        {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(getClass().getClassLoader().getResourceAsStream(filename)));

            Rhyme rhyme = new Rhyme();
            String line;
            while ((line = in.readLine()) != null)
            {
                if (StringUtils.isNotEmpty(line.trim()))
                {
                    rhyme.addLine(line);
                }
                else
                {
                    rhymes.add(rhyme);
                    rhyme = new Rhyme();
                }
            }
            in.close();
        }
        catch (Exception e)
        {
            throw new RuntimeException("could not load rhymes", e);
        }
    }

    public Rhyme getRandomRhyme()
    {
        return rhymes.get(random.nextInt(rhymes.size()));
    }

}

