package au.com.angry.irc.rhyme;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class Rhyme
{
    private List<String> lines = new ArrayList<String>();

    public void addLine(String line)
    {
        lines.add(line);
    }

    public String getLine(int l)
    {
        if (l > lines.size() - 1)
        {
            return null;
        }
        else
        {
            return lines.get(l);
        }
    }
}
