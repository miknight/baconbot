package au.com.angry.irc;

import au.com.angry.gatherer.*;

/**
 */
public class Gatherer
{
    private static DefaultGathererService service;

    private static GathererService getService() {
        service = new DefaultGathererService(new DefaultGathererHttpService());
        return service;
    }

    public static MagicCard getCard(String name)
    {
        return getService().getCard(name);
    }

    public static MagicCardSet getSet(String name)
    {
        return getService().getSet(name);
    }
}
