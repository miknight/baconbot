package au.com.angry.irc.things;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;
import com.google.common.base.Joiner;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;

/**
 * Created by cmiller on 3/06/2014.
 */
public class KarmaCommand implements Command
{
    private final ThingManager thingManager;

    public KarmaCommand(ThingManager thingManager)
    {
        this.thingManager = thingManager;
    }

    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        String name = Joiner.on(' ').join(args);
        if (StringUtils.isNotBlank(name))
        {
            Thing thing = thingManager.getThing(name);
            bot.sendMessage(event.getChannel(), thing.getName() + " has " + thing.getKarma() + " karma.");
            return true;
        }
        
        return false;
    }
}
