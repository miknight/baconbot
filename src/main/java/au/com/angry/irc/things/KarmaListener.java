package au.com.angry.irc.things;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cmiller on 2/06/2014.
 */
public class KarmaListener implements MessageListener
{
    private final ThingManager thingManager;
    private static final Pattern KARMA =  Pattern.compile("^(\\S+)(\\+\\+|--)$");

    public KarmaListener(ThingManager thingManager)
    {
        this.thingManager = thingManager;
    }

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        Matcher m = KARMA.matcher(event.getMessage().trim());

        if (m.find())
        {
            String name = m.group(1);
            String direction = m.group(2);

            Thing thing = thingManager.getThing(name);

            Thing newThing = new Thing(thing.getName(), adjustKarma(direction, thing.getKarma()), thing.getFacts());

            thingManager.setThing(name, newThing);
        }
    }

    private int adjustKarma(String direction, int karma)
    {
        if (direction.equals("--"))
            return karma - 1;

        if (direction.equals("++"))
            return karma + 1;

        return karma;
    }
}
