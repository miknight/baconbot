package au.com.angry.irc.things;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

import java.util.Arrays;
import java.util.Collections;

public class ForgetCommand implements Command
{
    private static final String BUILT_IN_VERB = "is";
    private static final String VERB_FACT = "is a definition verb";

    private final ThingManager thingManager;

    public ForgetCommand(ThingManager thingManager)
    {
        this.thingManager = thingManager;
    }

    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        bot.sendMessage(
                event.getChannel(),
                new Fact.Parser(thingManager).parse(args).map(new Function<Fact, String>()
                {
                    @Override
                    public String apply(Fact input)
                    {
                        if (forgetFact(input.getName(), input.getFact()))
                            return "Forgotten.";
                        else
                            return "I didn't know that to start with.";
                    }
                }).getOrElse("I did not recognise any verbs in that definition"));

        return true;
    }

    private boolean forgetFact(String name, String fact)
    {
        Thing thing = thingManager.getThing(name);

        if (Iterables.contains(thing.getFacts(), fact)) {
            thingManager.setThing(name, new Thing(thing.getName(),
                    thing.getKarma(),
                    Iterables.filter(thing.getFacts(), Predicates.not(Predicates.equalTo(fact)))));
            return true;
        }

        return false;
    }
}
