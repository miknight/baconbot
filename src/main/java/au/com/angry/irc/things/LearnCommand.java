package au.com.angry.irc.things;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;
import com.atlassian.fugue.Option;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;

public class LearnCommand implements Command
{
    private static final String BUILT_IN_VERB = "is";
    private static final String VERB_FACT = "is a definition verb";

    private final ThingManager thingManager;

    public LearnCommand(ThingManager thingManager)
    {
        this.thingManager = thingManager;
    }

    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        bot.sendMessage(
                event.getChannel(),
                new Fact.Parser(thingManager).parse(args).map(new Function<Fact, String>()
                {
                    @Override
                    public String apply(Fact input)
                    {
                        addFact(input);
                        return "Thanks, I will remember that";
                    }
                }).getOrElse("I did not recognise any verbs in that definition"));

        return true;
    }

    private void addFact(Fact fact)
    {
        Thing thing = thingManager.getThing(fact.getName());

        if (!Iterables.contains(thing.getFacts(), fact)) {
            thingManager.setThing(fact.getName(), new Thing(thing.getName(),
                    thing.getKarma(),
                    Iterables.concat(Collections.singleton(fact.getFact()), thing.getFacts())));
        }
    }

}
