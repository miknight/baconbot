package au.com.angry.irc.things;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

import java.util.Arrays;
import java.util.Collections;

public class ForgetAllCommand implements Command
{
    private static final String BUILT_IN_VERB = "is";
    private static final String VERB_FACT = "is a definition verb";

    private final ThingManager thingManager;

    public ForgetAllCommand(ThingManager thingManager)
    {
        this.thingManager = thingManager;
    }

    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        String name = Joiner.on(" ").join(args);
        Thing thing = thingManager.getThing(name);

        if (Iterables.isEmpty(thing.getFacts()))
        {
            bot.sendMessage(event.getChannel(), "I didn't know anything about " + thing);
            return false;
        }
        else
        {
            thingManager.setThing(name, new Thing(thing.getName(), thing.getKarma(), Collections.<String>emptyList()));
            bot.sendMessage(event.getChannel(), "I have erased " + thing.getName() + " from my mind");
            return true;
        }
    }
}
