package au.com.angry.irc.things;

import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

/**
 * Created by cmiller on 2/06/2014.
 */
public class Thing
{
    private final String name;
    private final int karma;
    private List<String> facts;

    public Thing(String name, int karma, Iterable<String> facts)
    {
        this.name = name;
        this.karma = karma;
        this.facts = Lists.newArrayList(facts);
    }

    public String getName()
    {
        return name;
    }

    public int getKarma()
    {
        return karma;
    }

    public Iterable<String> getFacts()
    {
        return Collections.unmodifiableCollection(facts);
    }
}
