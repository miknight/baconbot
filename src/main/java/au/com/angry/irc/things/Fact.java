package au.com.angry.irc.things;

import com.atlassian.fugue.Option;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import static com.atlassian.fugue.Option.*;

import java.util.Arrays;
import java.util.Collections;

public final class Fact
{
    public static class Parser {
        private static final String BUILT_IN_VERB = "is";
        private static final String VERB_FACT = "is a definition verb";

        private final Iterable<String> verbs;

        public Parser(ThingManager thingManager)
        {
            verbs = Iterables.concat(Collections.singleton(BUILT_IN_VERB), thingManager.getMatchingNames(VERB_FACT));
        }

        public Option<Fact> parse(String[] words) {
            Joiner j = Joiner.on(" ");

            if (words.length >= 3)
            {
                for (int i = 1; i < (words.length - 1); i++)
                {
                    String arg = words[i];

                    for (String verb : verbs)
                    {
                        if (verb.equals(arg))
                        {
                            return some(new Fact(j.join(Arrays.copyOfRange(words, 0, i)),
                                    j.join(Arrays.copyOfRange(words, i, words.length))));
                        }
                    }
                }
            }

            return none();
        }
    }

    private final String name;
    private final String fact;

    public Fact(String name, String fact)
    {
        this.name = name;
        this.fact = fact;
    }

    public String getName()
    {
        return name;
    }

    public String getFact()
    {
        return fact;
    }

    @Override
    public String toString()
    {
        return name + " " + fact;
    }
}
