package au.com.angry.irc.things;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by cmiller on 2/06/2014.
 */
public class ThingManager
{
    private final DBCollection coll;

    public ThingManager(DBCollection coll)
    {
        this.coll = coll;
    }

    public Thing getThing(String name)
    {
        DBCursor cur = findByName(name);

        if (cur.hasNext())
        {
            DBObject obj = cur.next();
            //noinspection unchecked
            return new Thing((String) obj.get("name"),
                    (Integer) obj.get("karma"),
                    (List<String>) obj.get("facts"));
        }
        else
        {
            return new Thing(name, 0, Collections.<String>emptyList());
        }
    }

    public Thing setThing(String oldName, Thing thing)
    {
        DBCursor cur = findByName(oldName);
        DBObject obj = new BasicDBObject();

        if (cur.hasNext())
            obj = cur.next();

        obj.put("name", thing.getName());
        obj.put("lcname", thing.getName().toLowerCase(Locale.ENGLISH));
        obj.put("karma", thing.getKarma());
        obj.put("facts", Lists.newArrayList(thing.getFacts()));

        coll.save(obj);
        return thing;
    }

    public Iterable<String> getMatchingNames(String fact) {
        DBObject query = new BasicDBObject();
        query.put("facts", fact);
        DBCursor cur = coll.find(query);


        List<String> results = new ArrayList<String>();
        while (cur.hasNext())
            results.add((String) cur.next().get("name"));

        return results;
    }

    private DBCursor findByName(String name)
    {
        DBObject query = new BasicDBObject();
        query.put("lcname", name.toLowerCase(Locale.ENGLISH));

        return coll.find(query);
    }
}
