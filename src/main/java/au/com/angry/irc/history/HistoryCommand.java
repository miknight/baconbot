package au.com.angry.irc.history;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.command.Command;
import au.com.angry.irc.listeners.MessageEvent;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class HistoryCommand implements Command
{
    private final DBCollection collection;

    public HistoryCommand(final DBCollection collection)
    {
        this.collection = collection;
    }

    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        String channel = event.getChannel();

        int count = 5;
        String nick = null;

        if (args.length > 0)
        {
            nick = args[0];
            if (nick.equals("help"))
            {
                bot.sendMessage(channel, "history nick [count]");
                return true;
            }
        }
        if (args.length > 1)
        {
            count = Integer.parseInt((args[1]));
            if (count < 1 || count > 15)
            {
                bot.sendMessage(channel, "count must be from 1 to 15");
                return true;
            }
        }

        BasicDBObject dbo = new BasicDBObject();
        dbo.put("timestamp", -1);

        BasicDBObject query = new BasicDBObject();
        if (nick != null)
            query.put("sender", nick);

        DBCursor cur = collection.find(query).sort(dbo);

        if (!cur.hasNext()) {
            if(nick!=null)
                bot.sendMessage(channel, "no history found for nick: "+nick);
            else
                bot.sendMessage(channel, "no history found");
        }

        while (cur.hasNext() && count > 0)
        {
            DBObject object = cur.next();
            String message = (String) object.get("message");
            String s = (String) object.get("sender");
            bot.sendMessage(channel, s + "> " + message);
            count--;
        }

        return true;
    }
}
