package au.com.angry.irc.history;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.MongoException;

/**
 */
public class HistoryListener implements MessageListener
{
    private final DBCollection collection;

    public HistoryListener(final DBCollection collection)
    {
        this.collection = collection;
    }

    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        try
        {
            BasicDBObject msg = new BasicDBObject();
            msg.put("channel", event.getChannel());
            msg.put("sender", event.getSender());
            msg.put("message", event.getMessage());
            msg.put("timestamp", System.currentTimeMillis());
            collection.insert(msg);
        }
        catch (Throwable t)
        {
            // ignore
        }
    }
}
