package au.com.angry.irc;

import twitter4j.auth.AccessToken;
import twitter4j.auth.OAuthSupport;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 */
public class ConnectionSettings
{
    private String server;
    private int port;
    private String username;
    private String password;
    private String fullname;
    private String nick;
    private String updatesUrl;
    private String channel;
    private boolean ssl;
    private boolean debug;

    private String oauthConsumerKey;
    private String oauthConsumerKeySecret;
    private String oauthAccessToken;
    private String oauthAccessTokenSecret;

    public ConnectionSettings(String filename)
    {
        try
        {
            Properties p = new Properties();
            FileInputStream in = new FileInputStream(new File(filename));
            p.load(in);
            loadProperties(System.getProperties());
            loadProperties(p);
            in.close();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Could not load properties file [ " + filename + " ]");
        }
    }

    public ConnectionSettings()
    {
        loadProperties(System.getProperties());
    }

    private void loadProperties(Properties p)
    {
        this.server = p.getProperty("connection.server", server);
        this.port = Integer.parseInt(p.getProperty("connection.port", String.valueOf(port)));
        this.username = p.getProperty("connection.username",username);
        this.password = p.getProperty("connection.password", password);

        this.oauthAccessToken = p.getProperty("oauth.accesstoken");
        this.oauthAccessTokenSecret = p.getProperty("oauth.accesstokensecret");
        this.oauthConsumerKey = p.getProperty("oauth.consumerkey");
        this.oauthConsumerKeySecret = p.getProperty("oauth.consumerkeysecret");

        this.fullname = p.getProperty("connection.fullname",fullname);
        this.nick = p.getProperty("connection.nick",nick);
        this.channel = p.getProperty("connection.channel","#anb");
        this.ssl = Boolean.parseBoolean(p.getProperty("connection.ssl","false"));
        this.debug = Boolean.parseBoolean(p.getProperty("connection.debug","false"));
        this.updatesUrl = p.getProperty("updates.url","http://wiki.angry.com.au/rest/wow/1.0/wow/activity.xml");
    }

    public String getServer()
    {
        return server;
    }

    public int getPort()
    {
        return port;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public String getFullname()
    {
        return fullname;
    }

    public String getNick()
    {
        return nick;
    }

    public boolean isSsl()
    {
        return ssl;
    }

    public String getUpdatesUrl()
    {
        return updatesUrl;
    }

    public String getChannel()
    {
        return channel;
    }

    public boolean isDebug()
    {
        return debug;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    public void setNick(String nick)
    {
        this.nick = nick;
    }

    public void setUpdatesUrl(String updatesUrl)
    {
        this.updatesUrl = updatesUrl;
    }

    public void setChannel(String channel)
    {
        this.channel = channel;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    public void setDebug(boolean debug)
    {
        this.debug = debug;
    }


    public void setTwitterAuthentication(OAuthSupport oauth)
    {
        oauth.setOAuthConsumer(oauthConsumerKey, oauthConsumerKeySecret);
        oauth.setOAuthAccessToken(new AccessToken(oauthAccessToken, oauthAccessTokenSecret));

    }
}
