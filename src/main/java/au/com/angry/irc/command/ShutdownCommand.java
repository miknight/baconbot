package au.com.angry.irc.command;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 */
public class ShutdownCommand implements Command
{
    private static Log log = LogFactory.getLog(CommandListener.class);
    public static final String URL = "http://launcher.worldofwarcraft.com/alert";
    public static final String START = "beginning on";
    public static final String END = "and we expect";

    // SERVERALERT: We will be performing scheduled maintenance
    // beginning on Tuesday, December 4, at 5:00 a.m. PST and we expect
    // the service to be available again at approximately 11:00 a.m. END. During this time the game will be unavailable for play.

    public static void main(String[] args) throws Exception {

//        DateFormat df = new SimpleDateFormat("EEEE MMMM d hh:mm zzz");
//        Date date = df.parse("Tuesday December 4 5:00 am END");
        DateFormat df = new SimpleDateFormat("EEEE MMMM d hh:mm a zzz yyyy");
        Date date = df.parse("Tuesday December 4 5:00 am PST 2012");

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        TimeZone zone = TimeZone.getTimeZone("Australia/Sydney");
        cal.setTimeZone(zone);

        Date time = cal.getTime();
        System.out.println(time.getTime()-date.getTime());
        System.out.println(new SimpleDateFormat("EEEE MMMM d hh:mm a yyyy").format(time));

    }

    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        if(args.length>0)
            return false;

        try
        {
            URL url = new URL(URL);
            String body = IOUtils.toString(url.openStream());

            bot.sendMessage(event.getChannel(), Jsoup.parse(body).text());

            // lets see if we can find the time
            if(body.contains(START)) {
                int start = body.indexOf(START)+ START.length();
                int end = body.indexOf(END);
                String dateString = body.substring(start, end).trim();
                dateString = dateString.replaceAll("\\.", "");
                dateString = dateString.replaceAll(",","");
                dateString = dateString.replaceAll(" at","");
                dateString = dateString+" "+Calendar.getInstance().get(Calendar.YEAR);

                DateFormat df = new SimpleDateFormat("EEEE MMMM d hh:mm a zzz yyyy");
                Date date = df.parse(dateString);

                TimeZone zone = TimeZone.getTimeZone("Australia/Sydney");
                Calendar cal = Calendar.getInstance(zone);
                cal.setTime(date);

                StringBuilder buf = new StringBuilder();
                buf.append("LOCALTIME: ");
                int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
                if(hourOfDay == 0) {
                    buf.append("midnight");
                } else if(hourOfDay<12) {
                    buf.append(hourOfDay).append("am");
                } else if(hourOfDay==12) {
                    buf.append("noon");
                } else {
                    buf.append(hourOfDay-12).append("pm");
                }


                bot.sendMessage(event.getChannel(), buf.toString());

            }

        }
        catch (Exception e)
        {
            log.error("error",e);
        }
        return true;
    }
}
