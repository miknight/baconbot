package au.com.angry.irc.command;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;

import java.io.File;
import java.io.IOException;

/**
 */
public class UpgradeCommand implements Command
{
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        if(args.length>0)
            return false;

        String channel = event.getChannel();

        File upgrade = new File(System.getProperty("work.dir","."),"upgrade");
        if(upgrade.exists()) {
            bot.sendMessage(channel, "upgrade flag already exists");
        } else {
            bot.sendMessage(channel, "ok, I'll be right back");
            try
            {
                upgrade.createNewFile();
                System.exit(1);
            }
            catch (IOException e)
            {
                bot.sendMessage(channel, "error: "+e.getMessage());
            }
        }

        return true;
    }
}
