package au.com.angry.irc.command;

import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Command Listener
 */
public class CommandListener implements MessageListener
{
    private static Log log = LogFactory.getLog(CommandListener.class);
    private ConnectionSettings settings;
    private static final String DELIM = ",:; ";
    private Map<String, Command> commands = new HashMap<String, Command>();

    public CommandListener(ConnectionSettings settings)
    {
        this.settings = settings;
    }

    public void addCommand(String str, Command command)
    {
        commands.put(str, command);
    }

    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String message = event.getMessage();
        String nickName = settings.getNick();
        String command = message;

        if (message.startsWith(nickName + ": "))
        {
            command = message.substring(message.indexOf(":")).trim();//.toLowerCase();

            while (DELIM.indexOf(command.charAt(0)) != -1)
            {
                command = command.substring(1);
            }
            processCommand(bot, event, command);
        }
    }

    private void processCommand(HoggerBot bot, MessageEvent event, String command)
    {
        String[] split = StringUtils.split(command);
        String[] args = new String[split.length - 1];
        System.arraycopy(split, 1, args, 0, split.length - 1);
        Command c = commands.get(split[0].toLowerCase());
        if (c != null)
        {
            log.info("command [ " + command + " ]");
            if (c.process(bot, event, split[0], args))
            {
                event.consume();
            }
        }
    }
}
