package au.com.angry.irc.command;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.jokes.Fact;
import au.com.angry.irc.listeners.MessageEvent;

/**
 */
public class FactCommand implements Command
{
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        if(args.length==0) {
            bot.sendMessage(event.getChannel(), Fact.getFact());
            return true;
        }
        return false;
    }
}
