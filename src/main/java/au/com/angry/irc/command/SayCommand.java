package au.com.angry.irc.command;

import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;

/**
 */
public class SayCommand implements Command
{
    private String channel;

    public SayCommand(ConnectionSettings settings)
    {
        channel = settings.getChannel();
    }

    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        StringBuilder b = new StringBuilder();
        for (String arg : args)
        {
            b.append(arg);
            b.append(" ");
        }
        bot.sendMessage(channel,b.toString().trim());
        event.consume();
        return false;
    }
}
