package au.com.angry.irc.command;

import au.com.angry.irc.Converter;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.Setting;
import au.com.angry.irc.listeners.MessageEvent;

import java.util.Set;

public class SetCommand implements Command
{
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        String channel = event.getChannel();

        if (args.length < 1 || args.length > 2)
            return false;

        String key = args[0];
        Setting setting = bot.getConfiguration(key);
        if (setting == null)
            return false;

        if (args.length == 2)
        {
            String newValue = args[1];
            Converter converter = setting.getConverter();

            if (!converter.isValid(newValue))
            {
                bot.sendMessage(channel, key + " " + converter.getConstraint());
                return true;
            }

            bot.updateConfiguration(key, newValue);
        }

        bot.sendMessage(channel, key + " = " + bot.getConfiguration(key).get());
        return true;
    }
}