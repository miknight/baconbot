package au.com.angry.irc.command;

import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.bacon.AbuseListener;
import au.com.angry.irc.bacon.Shaker;
import au.com.angry.irc.listeners.MessageEvent;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 */
public class AbuseCommand implements Command
{
    private final ConnectionSettings settings;
    private final AbuseListener abuseListener;

    public AbuseCommand(ConnectionSettings settings, AbuseListener abuseListener)
    {
        this.settings = settings;
        this.abuseListener = abuseListener;
    }

    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        if (args.length == 1)
        {
            String target = args[0];
            if(target.startsWith("@")) {
                try
                {
                    TwitterFactory.getSingleton().updateStatus(target+" "+Shaker.getAbuse());
                }
                catch (TwitterException e)
                {
                    bot.doSend("error sending tweet: "+e.getLocalizedMessage());
                }
            } else {
                bot.sendMessage(event.getChannel(), "now abusing " + target);
                bot.sendMessage(settings.getChannel(), target + ": " + Shaker.getAbuse());
                abuseListener.setTarget(target);
            }
            return true;
        }
        else if(args.length==0)
        {
            bot.sendMessage(event.getChannel(), "abusing " + abuseListener.getTarget());
            return true;
        }

        return false;
    }
}
