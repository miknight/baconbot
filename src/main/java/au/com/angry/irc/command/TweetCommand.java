package au.com.angry.irc.command;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import org.apache.commons.lang.StringUtils;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 */
public class TweetCommand implements Command
{
    public TweetCommand()
    {
    }

    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        try
        {
            TwitterFactory.getSingleton().updateStatus(StringUtils.join(args," "));
        }
        catch (TwitterException e)
        {
            return false;
        }
        return true;
    }
}
