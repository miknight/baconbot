package au.com.angry.irc.command;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ChangesCommand implements Command
{
    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        return doChanges(bot);
    }

    public static boolean doChanges(HoggerBot bot)
    {
        try
        {
            File changes = new File(System.getProperty("changes"));
            BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(changes)));
            String line = null;
            int lines = 0;
            while ((line = r.readLine()) != null && lines < 10)
            {
                if (!line.isEmpty())
                {
                    lines++;
                    bot.doSend(line);
                }
            }
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
