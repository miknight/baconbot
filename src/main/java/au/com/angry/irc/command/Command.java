package au.com.angry.irc.command;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;

/**
 */
public interface Command
{
    boolean process(HoggerBot bot, MessageEvent event, String command, String[] args);
}
