package au.com.angry.irc.command;

import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import org.apache.commons.lang.math.RandomUtils;

/**
 * Roll a dice!!
 */
public class RollCommand implements Command
{
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        if(args.length==0) {
            bot.reply(event,"you rolled "+ RandomUtils.nextInt(100)+1);
            return true;
        }
        return false;
    }
}
