package au.com.angry.irc.command;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ShortlogCommand implements Command
{
    @Override
    public boolean process(HoggerBot bot, MessageEvent event, String command, String[] args)
    {
        try
        {
            File changes = new File(System.getProperty("shortlog"));
            BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(changes)));
            String line = null;
            int lines = 0;
            while ((line = r.readLine()) != null && lines < 20)
            {
                if (!line.isEmpty())
                {
                    lines++;
                    bot.doSend(line);
                }
            }
            return true;
        }
        catch (Exception e)
        {
            // ignore (you get no changes!)
        }
        return false;
    }
}
