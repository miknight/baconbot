package au.com.angry.irc;

public class Setting
{
    private final Converter converter;
    private final Object value;

    public Setting(Converter converter, Object value)
    {
        this.converter = converter;
        this.value = value;
    }

    public Object get()
    {
        return value;
    }

    public Converter getConverter()
    {
        return converter;
    }
}
