package au.com.angry.irc.youtube;

import org.apache.commons.lang.StringUtils;

import java.util.List;

public class VideoData
{
    private final String title;
    private final List<String> categories;
    private final String videoId;

    public VideoData(String videoId, String title, List<String> categories) {
        this.title = title;
        this.categories = categories;
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getShortUrl() {
        return "http://youtu.be/" + videoId;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append(getShortUrl()).append(" - ").append(title);

        if (!categories.isEmpty())
            buf.append(" (").append(StringUtils.join(categories, ", ")).append(")");

        return buf.toString();
    }
}
