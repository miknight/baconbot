package au.com.angry.irc.youtube;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YoutubeListener implements MessageListener
{
    private static final Pattern[] PATTERNS = new Pattern[]{
            Pattern.compile(".*youtube.com/watch.*[\\?&]v=([A-Za-z0-9_\\-]+).*"),
            Pattern.compile(".*youtu.be/([A-Za-z0-9_\\-]+).*")};

    public static void main(String[] args) throws InterruptedException {
        VideoData lastVideoData = getLastVideoData();
        System.out.println(lastVideoData.getTitle());
    }

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String sender = event.getSender();
        String message = event.getMessage();

        if (bot.getNick().equalsIgnoreCase(sender))
            return;

        VideoData data = getDataFromMessage(message);

        if (data != null && data.getTitle().toLowerCase().contains("bieber")) {
            VideoData lastVideoData = getLastVideoData();
            data = new VideoData(data.getVideoId(), lastVideoData.getTitle(), lastVideoData.getCategories());
        }

        if (data != null) {
            bot.sendMessage(getTarget(event.getChannel(), sender), data.toString());
            event.consume();
        }
    }

    private String getTarget(String channel, String sender) {
        return StringUtils.isNotBlank(channel) ? channel : sender;
    }


    private static VideoData getDataFromMessage(String message) {
        String videoId = getVideoId(message);
        if (videoId != null) {
            return getVideoData(videoId);
        }
        return null;
    }

    private static VideoData getVideoData(String videoId) {
        InputStream in = null;
        HttpURLConnection conn = null;
        try {
            URL dataUrl = new URL("http://gdata.youtube.com/feeds/api/videos/" + videoId);
            conn = (HttpURLConnection) dataUrl.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            in = conn.getInputStream();

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(in);

            String title = "Unknown Title";
            List<String> categories = new ArrayList<String>();

            NodeList titles = doc.getElementsByTagName("title");
            if (titles.getLength() > 0)
                title = titles.item(0).getTextContent();

            NodeList catnodes = doc.getElementsByTagName("media:category");
            for (int i = 0; i < catnodes.getLength(); i++) {
                categories.add(catnodes.item(i).getTextContent());
            }

            return new VideoData(videoId, title, categories);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } finally {
            if (in != null) try {
                in.close();
            } catch (IOException e) {
            }
            if (conn != null) conn.disconnect();
        }

        return null;
    }

    private static VideoData getLastVideoData() {
        InputStream in = null;
        HttpURLConnection conn = null;
        try {
            URL dataUrl = new URL("http://gdata.youtube.com/feeds/api/videos/");
            conn = (HttpURLConnection) dataUrl.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            in = conn.getInputStream();

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(in);

            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();

            NodeList set = (NodeList) xpath.compile("/feed/entry").evaluate(doc, XPathConstants.NODESET);
            Node node = set.item(new Random().nextInt(set.getLength()));
            
            String str = xpath.compile("id").evaluate(node);
            return getVideoData(str.substring(str.lastIndexOf("/")+1));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } finally {
            if (in != null) try {
                in.close();
            } catch (IOException e) {
            }
            if (conn != null) conn.disconnect();
        }

        return null;

    }

    private static String getVideoId(String message) {
        for (Pattern pattern : PATTERNS) {
            Matcher matcher = pattern.matcher(message);
            if (matcher.matches())
                return matcher.group(1);
        }

        return null;
    }
}
