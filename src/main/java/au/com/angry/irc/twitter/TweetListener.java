package au.com.angry.irc.twitter;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import org.apache.commons.lang.StringUtils;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TweetListener implements MessageListener
{
    private static final Pattern[] PATTERNS =  new Pattern[] {
            Pattern.compile(".*twitter.com.*/([0-9]+).*"),
            Pattern.compile(".*twitter.com.*/[A-Za-z0-9_]{1,15}/([0-9]+).*"),
            Pattern.compile(".*twitter.com.*/[A-Za-z0-9_]{1,15}/status/([0-9]+).*")
    };

    private Twitter twitter;

    public TweetListener() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setTweetModeExtended(true);
        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter = tf.getInstance();
    }

    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String sender = event.getSender();
        String message = event.getMessage();
        String channel = event.getChannel();

        if (bot.getNick().equalsIgnoreCase(sender))
            return;

        try {
            Status data = getDataFromMessage(message);
            if (data != null) {
                String[] lines = StringUtils.split(data.getText(),"\n");
                if(lines.length==1) {
                    bot.sendMessage(getTarget(channel, sender), lines[0]);
                } else {
                    for (String line : lines)
                        bot.sendMessage(getTarget(channel, sender), "| " + line);
                }
                event.consume();
            }
        } catch (IllegalStateException e) {
            bot.sendMessage(getTarget(channel, sender), e);
        } catch (TwitterException e) {
            throw new RuntimeException(e);
        }
    }

    private Status getDataFromMessage(String message) throws TwitterException {
        String tweetId = getTweetId(message);
        try {
            long tweetIdLong = Long.parseLong(tweetId);
            return twitter.showStatus(tweetIdLong);
        } catch (NumberFormatException e) {
            // ignore, not a tweet
            return null;
        } catch (TwitterException e) {
            throw e;
        }
    }

    private static String getTweetId(String message) {
        for (Pattern pattern : PATTERNS) {
            Matcher matcher = pattern.matcher(message);
            if (matcher.matches())
                return matcher.group(1);
        }

        return null;
    }
    private String getTarget(String channel, String sender) {
        return StringUtils.isNotBlank(channel) ? channel : sender;
    }
}
