package au.com.angry.irc;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

/**
 */
public abstract class IrcJob implements Job
{
    public final void execute(JobExecutionContext ctx) throws JobExecutionException
    {
        HoggerBot bot = (HoggerBot) ctx.getJobDetail().getJobDataMap().get("bot");
        Date prev = ctx.getPreviousFireTime();
        execute(bot,prev);
    }

    protected abstract void execute(HoggerBot bot, Date prev);

}