package au.com.angry.irc;

import au.com.angry.irc.bacon.AbuseListener;
import au.com.angry.irc.bacon.FactsListener;
import au.com.angry.irc.chatterbotapi.ChatterBotUserSessions;
import au.com.angry.irc.command.*;
import au.com.angry.irc.game.GuessCommand;
import au.com.angry.irc.game.AskCommand;
import au.com.angry.irc.game.StartGameCommand;
import au.com.angry.irc.game.TwentyQuestionsListener;
import au.com.angry.irc.history.HistoryListener;
import au.com.angry.irc.listeners.*;
import au.com.angry.irc.lists.ListsCommand;
import au.com.angry.irc.lists.ListsListener;
import au.com.angry.irc.lists.PersonListManager;
import au.com.angry.irc.things.ForgetAllCommand;
import au.com.angry.irc.things.ForgetCommand;
import au.com.angry.irc.things.KarmaCommand;
import au.com.angry.irc.things.KarmaListener;
import au.com.angry.irc.things.LearnCommand;
import au.com.angry.irc.things.ThingManager;
import au.com.angry.irc.twitter.TweetListener;
import au.com.angry.irc.youtube.YoutubeListener;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jibble.pircbot.PircBot;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import twitter4j.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.*;

/**
 */
public class HoggerBot extends PircBot
{

    public static void main(String[] args) throws Exception
    {
        ConnectionSettings settings = new ConnectionSettings(args[0]);
        try
        {
            settings.setTwitterAuthentication(TwitterFactory.getSingleton());
            settings.setTwitterAuthentication(TwitterStreamFactory.getSingleton());
        }
        catch (Exception e)
        {
            // probably running in dev - so lets continue
            e.printStackTrace();
        }
        HoggerBot bot = new HoggerBot(settings);
        bot.connect();
    }

    private static Log log = LogFactory.getLog(HoggerBot.class);
    private ConnectionSettings settings;
    private Date lastUpdatesDate;
    private BackoffCounter backoffCounter = new BackoffCounter();
    private final List<MessageListener> listeners = new ArrayList<MessageListener>();
    private final Random random = new Random();
    private Map<String, Setting> configuration = new HashMap<String, Setting>();
    private final DBCollection historyCollection;

    private final Map<String, Object> session = new HashMap<String, Object>();

    public HoggerBot(ConnectionSettings settings) throws UnknownHostException
    {
        this.settings = settings;
//        DB mongodb = new Mongo("172.17.8.101").getDB("hogger");
        DB mongodb = new Mongo("127.0.0.1").getDB("hogger");

        this.historyCollection = mongodb.getCollection("hogger");

        ThingManager thingManager = new ThingManager(mongodb.getCollection("things"));
        PersonListManager listManager = new PersonListManager(mongodb.getCollection("personlists"));

        setName(settings.getNick());
        setVerbose(true);
        CommandListener commandListener = new CommandListener(settings);
        listeners.add(commandListener);
        listeners.add(new HistoryListener(this.historyCollection));
        listeners.add(new CardListener());
        listeners.add(new DefineListener(thingManager));
        listeners.add(new FactsListener(settings, "bacon.txt", "bacon", new Chance(0.25, 1.1)));
        listeners.add(new FactsListener(settings, "coffee.txt", "coffee", new Chance(0.25, 1.1)));
        listeners.add(new FactsListener(settings, "virtualwolf.txt", "virtualwolf", new Chance(0.05, 1.05)));
        listeners.add(new FactsListener(settings, "furgin.txt", "furgin", new Chance(0.05, 1.05)));
        listeners.add(new YoutubeListener());
        listeners.add(new TweetListener());
        listeners.add(new MiscListener(settings));
        listeners.add(new WelcomeListener(settings, thingManager));
        listeners.add(new ComboListener());
        listeners.add(new TwentyQuestionsListener());
        listeners.add(new KarmaListener(thingManager));
        listeners.add(new ListsListener(listManager));
        listeners.add(new Imgur());
        AbuseListener abuseListener = new AbuseListener();
        commandListener.addCommand("abuse", new AbuseCommand(settings, abuseListener));
        listeners.add(abuseListener);
        commandListener.addCommand("upgrade", new UpgradeCommand());
        commandListener.addCommand("set", new SetCommand());
        commandListener.addCommand("joke", new JokeCommand());
        commandListener.addCommand("fact", new FactCommand());
        commandListener.addCommand("shutdown?", new ShutdownCommand());
        commandListener.addCommand("game", new StartGameCommand());
        commandListener.addCommand("guess", new GuessCommand());
        commandListener.addCommand("ask", new AskCommand());
        commandListener.addCommand("roll", new RollCommand());
        commandListener.addCommand("say", new SayCommand(settings));
        commandListener.addCommand("tweet", new TweetCommand());
        commandListener.addCommand("shortlog", new ShortlogCommand());
        commandListener.addCommand("changes", new ChangesCommand());
        commandListener.addCommand("karma", new KarmaCommand(thingManager));
        commandListener.addCommand("learn", new LearnCommand(thingManager));
        commandListener.addCommand("forget", new ForgetCommand(thingManager));
        commandListener.addCommand("forgetall", new ForgetAllCommand(thingManager));
        commandListener.addCommand("lists", new ListsCommand(listManager));

        listeners.add(new SkyrimListener());
        registerConfiguration("verbosity", new Setting(new ProbabilityConverter(), 0.25));
//        commandListener.addCommand("history", new HistoryCommand(historyCollection));

        ChatterBotUserSessions userSessions = new ChatterBotUserSessions(settings);
        listeners.add(userSessions);

        startScheduler();
        try {
            startTwitterStream(userSessions);
        } catch (Exception e) {
            log.error("not starting twitter", e);
        }
    }

    private void startScheduler()
    {
        try
        {
            SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
            Scheduler sched = schedFact.getScheduler();
            sched.start();

            JobDataMap data = new JobDataMap();
            data.put("bot", this);

//            ActivityJob.register(sched, data);
            ReconnectJob.register(sched, data);
        }
        catch (SchedulerException e)
        {
            log.error("could not start quartz sheduler", e);
        }
    }

    private void startTwitterStream(final ChatterBotUserSessions userSessions)
    {
        TwitterStream stream = TwitterStreamFactory.getSingleton();
        stream.addListener(new UserStreamAdapter()
        {
            public void onStatus(Status status)
            {
                String from = status.getUser().getScreenName();
                String text = status.getText();
                doSend("(@" + from + ") " + text);
                String me = "h0gger";
                if (!me.equalsIgnoreCase(from) && !status.isRetweet())
                {
                    try
                    {
                        String message = removeMentions(text).trim();
                        StatusUpdate reply = new StatusUpdate("@"+from + " "+userSessions.getSession(from).think(message));
                        reply.setInReplyToStatusId(status.getId());
                        TwitterFactory.getSingleton().updateStatus(reply);
                    }
                    catch (Exception e)
                    {
                        doSend("error: " + e.getMessage());
                    }
                }
            }
        });
        stream.user(new String[]{"@h0gger"});
    }

    public static String extractMentions(String text)
    {
        StringBuilder b = new StringBuilder();
        boolean extracting = false;
        for (int i = 0; i < text.length(); i++)
        {
            char c = text.charAt(i);
            if (!extracting && c == '@')
            {
                extracting = true;
            }
            else if (extracting && c == ' ')
            {
                extracting = false;
                b.append(' ');
            }

            if (extracting)
                b.append(c);

        }
        return b.toString();
    }

    public static String removeMentions(String text)
    {
        StringBuilder b = new StringBuilder();
        boolean extracting = false;
        for (int i = 0; i < text.length(); i++)
        {
            char c = text.charAt(i);
            if (!extracting && c == '@')
            {
                extracting = true;
            }
            else if (extracting && c == ' ')
            {
                extracting = false;
            }

            if (!extracting)
                b.append(c);

        }
        return b.toString().trim();
    }

    protected void onPrivateMessage(String sender, String login, String hostname, String message)
    {
        send(new MessageEvent(this, sender, sender, message));
    }

    private void send(MessageEvent e)
    {
        for (MessageListener listener : listeners)
        {
            if (e.isConsumed())
                break;

            try {
                listener.onMessage(this, e);
            } catch (Exception ex) {
                log.error("error sending message: "+ex.getMessage(),ex);
            }
        }
    }

    protected void onMessage(String channel, String sender, String login, String hostname, String message)
    {
        send(new MessageEvent(this, sender, channel, message));
    }

    public void doSend(String message)
    {
        sendMessage(settings.getChannel(), message);
    }

    public void reply(MessageEvent event, String message)
    {
        sendMessage(settings.getChannel(), event.getSender() + ": " + message);
    }

    private boolean isSufficientlyVerbose()
    {
        return random.nextDouble() < (Double) getConfiguration("verbosity").get();
    }

    /**
     * Sends a message that we really don't care about. A back-off algorithm will swallow more messages the more
     * are being generated.
     *
     * @param target  who to send the message to (person or channel)
     * @param message the message to send.
     */
    public void sendOptionalMessage(String target, String message)
    {
        if (isSufficientlyVerbose() && backoffCounter.shouldSend())
            sendMessage(target, message);
    }

    public Date getLastUpdatesDate()
    {
        return lastUpdatesDate;
    }

    public void setLastUpdatesDate(Date lastUpdatesDate)
    {
        this.lastUpdatesDate = lastUpdatesDate;
    }

    public ConnectionSettings getSettings()
    {
        return settings;
    }

    protected void onKick(String channel, String kickerNick, String kickerLogin, String kickerHostname, String recipientNick, String reason)
    {
        joinChannel(settings.getChannel());
    }

    protected void onPart(String channel, String sender, String login, String hostname)
    {
        joinChannel(settings.getChannel());
    }

    protected void onFinger(String sourceNick, String sourceLogin, String sourceHostname, String target)
    {
        sendMessage(settings.getChannel(), sourceNick + " just fingered me.");
    }

    protected void onJoin(String channel, String sender, String login, String hostname)
    {
        send(new JoinMessageEvent(this, sender, channel, sender + " has joined " + channel));
    }

    public void registerConfiguration(String key, Setting value)
    {
        configuration.put(key, value);
    }

    public Setting getConfiguration(String key)
    {
        return configuration.get(key);
    }

    public void updateConfiguration(String key, String value)
    {
        Setting s = configuration.get(key);
        Converter c = s.getConverter();
        configuration.put(key, new Setting(c, c.convert(value)));
    }

    public void connect()
    {
        try
        {

            connect(settings.getServer(), settings.getPort());
            joinChannel();
            ChangesCommand.doChanges(this);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public void joinChannel()
    {
        joinChannel(settings.getChannel());
    }

    public Set<String> getConfigurationKeys()
    {
        return configuration.keySet();
    }

    protected void onAction(String sender, String login, String hostname, String target, String action)
    {
        send(new ActionEvent(this, sender, target, sender + " " + action));
    }

    public Object get(String key)
    {
        return session.get(key);
    }

    public void set(String key, Object value)
    {
        session.put(key, value);
    }

    public boolean contains(String key)
    {
        return session.containsKey(key);
    }

    public void clear(String key)
    {
        session.remove(key);
    }

    public void sendMessage(String target, Exception e)
    {
        StringWriter buf = new StringWriter();
        e.printStackTrace(new PrintWriter(buf, true));
        String[] lines = StringUtils.split(buf.toString(), "\n");
        for (String line : lines)
        {
            System.out.println("\"" + ((int) line.charAt(0)) + "\"");
            if (line.charAt(0) != '\t' || line.contains("angry"))
            {
                line = line.replaceAll("\t", "    ");
                sendMessage(target, line);
            }
        }
    }
}
