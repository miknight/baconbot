package au.com.angry.irc.bacon;

import au.com.angry.irc.Chance;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;

/**
 * http://www.pangloss.com/seidel/Shaker/index.html
 */
public class AbuseListener implements MessageListener
{

    private String target;
    private final Facts facts;
    private final Chance chance;
    private final Chance remoteChance;

    public AbuseListener() {
        this.facts = new Facts("abuse.txt");
        this.chance = new Chance(0.95d);
        this.remoteChance = new Chance(0.80d);
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.chance.reset();
        this.target = target;
    }

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        if (target!=null && event.getSender().equals(target) && chance.isTrue()) {

            if (remoteChance.isTrue()) {
                bot.sendOptionalMessage(event.getChannel(), target + ": " + Shaker.getAbuse());
            } else {
                String fact = facts.getRandomFact();
                fact = fact.replaceAll("\\{nick}", event.getSender());
                fact = fact.replaceAll("\\{message}", event.getMessage());
                bot.sendOptionalMessage(event.getChannel(), fact);
            }

        }
    }
}
