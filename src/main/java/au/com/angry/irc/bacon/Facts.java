package au.com.angry.irc.bacon;

import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 */
public class Facts
{
    private List<String> facts = new ArrayList<String>();
    private final Random random = new Random();

    public Facts(String filename)
    {
        try
        {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(getClass().getClassLoader().getResourceAsStream(filename)));
            String fact;
            while ((fact = in.readLine()) != null)
            {
                if (StringUtils.isNotEmpty(fact))
                    facts.add(fact);
            }
            in.close();
        }
        catch (Exception e)
        {
            throw new RuntimeException("could not load facts", e);
        }
    }

    public String getRandomFact()
    {
        return facts.get(random.nextInt(facts.size()));
    }
}
