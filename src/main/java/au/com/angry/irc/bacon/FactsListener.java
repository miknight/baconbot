package au.com.angry.irc.bacon;

import au.com.angry.irc.Chance;
import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Random;

/**
 * Listens and responds to messages.
 */
public class FactsListener implements MessageListener
{
    private static Log log = LogFactory.getLog(FactsListener.class);
    private final String channel;
    private final String nick;
    private final String msg;
    private final Facts facts;
    private final Chance chance;

    public FactsListener(ConnectionSettings settings, String resource, String msg, Chance chance)
    {
        this.chance = chance;
        this.channel = settings.getChannel();
        this.nick = settings.getNick();
        this.msg = msg;
        this.facts = new Facts(resource);
    }

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String sender = event.getSender();
        String message = event.getMessage();

        if ((sender.toLowerCase().contains(this.msg) || message.toLowerCase().contains(this.msg)) &&
                (this.channel.equals(channel) && !this.nick.equals(sender)))
        {
            if (chance.isTrue())
                bot.sendOptionalMessage(channel, facts.getRandomFact());
        }

    }
}