package au.com.angry.irc;

public interface Converter<T>
{
    boolean isValid(String s);

    String getConstraint();

    T convert(String s) throws IllegalArgumentException;
}
