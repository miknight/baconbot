package au.com.angry.irc.jokes;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URL;

/**
 */
public class Joke
{
    public static String getJoke()
    {
        try
        {
            Document document = Jsoup.parse(new URL("http://www.onelinerz.net/random-one-liners/1/"), 500);
            return document.select("div.oneliner").html();
        }
        catch (Exception e)
        {
            return "stfu";
        }
    }
}
