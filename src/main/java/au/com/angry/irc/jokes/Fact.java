package au.com.angry.irc.jokes;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URL;

/**
 */
public class Fact
{
    public static String getFact()
    {
        try
        {
            Document document = Jsoup.parse(new URL("http://www.randomfunfacts.com/"), 500);
            return document.select("table#AutoNumber1 tbody tr td font strong").text().trim();
        }
        catch (Exception e)
        {
            return "stfu";
        }
    }
}
