package au.com.angry.irc.chatterbotapi;

import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.listeners.ActionEvent;
import au.com.angry.irc.listeners.MessageEvent;
import au.com.angry.irc.listeners.MessageListener;
import com.google.code.chatterbotapi.ChatterBotFactory;
import com.google.code.chatterbotapi.ChatterBotSession;
import com.google.code.chatterbotapi.ChatterBotType;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import org.apache.commons.lang.StringUtils;

import java.util.Random;
import java.util.concurrent.*;

/**
 * Tracks multiple sessions against different IRC Users
 */
public class ChatterBotUserSessions implements MessageListener
{
    private class Speak implements Runnable {
        private final long time;
        private MessageEvent event;

        private Speak(final MessageEvent event)
        {
            this.event = event;
            this.time = System.currentTimeMillis();
        }

        public void run()
        {
            String message = event.getMessage();
            String channel = event.getChannel();
            String sender = event.getSender();
            String directedMessage = getDirectedMessage(sender, message);

            String response = "";
            if(!sender.equals(nick) && event instanceof ActionEvent && directedMessage!=null)
                response = spoke(sender, directedMessage);

            if(message.startsWith(nick+":") || !channel.startsWith("#"))
                response = spoke(sender, message);

            if(StringUtils.isNotEmpty(response)) {
                // don't want it to take more than 2000ms for a response if we can help it
                long max = 2000;
                long waited = System.currentTimeMillis()-time;
                long waitFor = (waited>max) ? 0 : random.nextInt((int)Math.max(1,(max-waited)));
                service.schedule(new Reply(event,response),waitFor,TimeUnit.MILLISECONDS);
            }
        }
    }

    private class Reply implements Runnable {
        private final String response;
        private MessageEvent event;

        private Reply(final MessageEvent event, final String response)
        {
            this.event = event;
            this.response = response;
        }

        public void run()
        {
            String[] array = response.split("<br>");
            for (String s : array)
                if(s.trim().length()>0) event.reply(s.trim());
        }
    }

    final Cache<String, ChatterBotSession> sessions = CacheBuilder.newBuilder()
            .concurrencyLevel(4)
            .maximumSize(10000)
            .expireAfterAccess(10, TimeUnit.MINUTES)
            .build(
                    new CacheLoader<String, ChatterBotSession>()
                    {
                        public ChatterBotSession load(String key) throws Exception
                        {
//                            return new ChatterBotFactory().create(ChatterBotType.PANDORABOTS, "ed4e043cde343ab0").createSession();
                            return new ChatterBotFactory().create(ChatterBotType.PANDORABOTS, "85056b4a9e340552").createSession();
                        }
                    });

    private final Random random = new Random();
    private final ScheduledExecutorService service = Executors.newScheduledThreadPool(5);
    private final String nick;

    public ChatterBotUserSessions(ConnectionSettings settings)
    {
        nick = settings.getNick();
    }

    public void onMessage(final HoggerBot bot, final MessageEvent event)
    {
        // run this in a thread pool because when cleverbot is down this can block all of hogger
        service.submit(new Speak(event));
    }

    /**
     * If the action is in the form "X verb* hogger" then turn it in to "hogger: I verb* you."
     * @return
     */
    public String getDirectedMessage(String sender, String message) {
        String words[] = StringUtils.split(message, " ");

        if(words.length<3)
            return null;

        String possibleTarget = words[words.length - 1].toLowerCase().replaceAll("\\.","");
        if(possibleTarget.equals(nick)) {
            StringBuilder buf = new StringBuilder();

            buf.append(nick).append(":"); // hogger:
            // verb*
            for(int i=0;i<words.length-1;i++) {
                buf.append(" ");
                buf.append(words[i]);
            }
            buf.append(" you."); // you

            return buf.toString();
        } else {
            return null;
        }
    }

    private String spoke(String sender, String message)
    {
        if(message.contains(nick+":"))
            message = message.substring((nick+":").length()).trim();

        try
        {
            ChatterBotSession session = getSession(sender);
            String result = session.think(message);
            return result.trim();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public ChatterBotSession getSession(String sender)
    {
        try
        {
            return sessions.get(sender);
        }
        catch (ExecutionException e)
        {
            throw new RuntimeException(e);
        }
    }
}
