package au.com.angry.irc.listeners;

import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.things.Thing;
import au.com.angry.irc.things.ThingManager;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

/**
 */
public class WelcomeListener implements MessageListener
{
    private static final Random rand = new Random();
    private static final String WELCOMES[] = {
        "Hello!", "Welcome back", "Hi", "What's doin'?", "Got any grapes?", "Heredue!", "Welcone!"
    };

    private final ConnectionSettings settings;
    private final ThingManager thingManager;

    public WelcomeListener(ConnectionSettings settings, ThingManager thingManager)
    {
        this.settings = settings;
        this.thingManager = thingManager;
    }

    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        if(!event.getSender().equals(settings.getNick()) && event instanceof JoinMessageEvent)
        {
            List<String> facts = Lists.newArrayList(thingManager.getThing(event.getSender()).getFacts());

            int idx = rand.nextInt(WELCOMES.length + facts.size());

            if (idx < WELCOMES.length)
                bot.sendOptionalMessage(event.getChannel(), event.getSender() + ": " + WELCOMES[idx]);
            else
                bot.sendOptionalMessage(event.getChannel(), event.getSender() + " " + facts.get(idx - WELCOMES.length));
        }
    }
}
