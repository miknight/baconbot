package au.com.angry.irc.listeners;

import au.com.angry.irc.HoggerBot;

/**
 * COMBO!
 */
public class ComboListener implements MessageListener
{
    private long comboStart = 0;
    private String lastMessage = null;
    private int count;
    private boolean participated = false;

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String message = event.getMessage();
        if (lastMessage == null || !lastMessage.equals(message))
        {
            count = 1;
            lastMessage = message;
            comboStart = System.currentTimeMillis();
            participated = false;
        }
        else if (lastMessage.equals(message))
        {
            count++;
        }

        if (!participated && count >= 3 && System.currentTimeMillis() - comboStart < 30000)
        {
            bot.sendOptionalMessage(event.getChannel(), lastMessage);
            participated = true;
        }
    }
}
