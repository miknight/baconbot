package au.com.angry.irc.listeners;

import au.com.angry.irc.HoggerBot;
import au.com.angry.irc.things.Thing;
import au.com.angry.irc.things.ThingManager;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 */
public class DefineListener implements MessageListener
{
    private final ThingManager thingManager;
    private final Random random = new Random();

    public static final String PREFIX = "define: ";

    public DefineListener(ThingManager thingManager)
    {
        this.thingManager = thingManager;
    }

    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String message = event.getMessage().replaceAll("([\u0000-\u001F]|\u007F)", "");
        String channel = event.getChannel();
        if (message.startsWith(PREFIX))
        {
            String word = message.substring(PREFIX.length());

            if (!tryThingManager(bot, channel, word))
            {
                if (!tryWiktionary(bot, channel, word))
                {
                    tryUrbanDictionary(bot, channel, word);
                }
            }
        }
    }

    private boolean tryThingManager(HoggerBot bot, String channel, String word)
    {
        Thing thing = thingManager.getThing(word);
        if (!Iterables.isEmpty(thing.getFacts()))
        {
            List<String> facts = Lists.newArrayList(thing.getFacts());
            String randomFact = facts.get(random.nextInt(facts.size()));
            bot.sendMessage(channel, thing.getName() + " " + randomFact);

            return true;
        }

        return false;
    }

    private boolean tryWiktionary(HoggerBot bot, String channel, String word)
    {
        word = word.toLowerCase();

        try
        {
            String cont;
            String template = "http://en.wiktionary.org/w/api.php?action=query&titles=%s&format=json&prop=extracts&explaintext=true&exsectionformat=wiki";
            String url;

            String encodedWord = URLEncoder.encode(word, "UTF-8");
            cont = "http://en.wiktionary.org/wiki/" + encodedWord;
            url = String.format(template, encodedWord);

            JSONObject result = (JSONObject) JSONValue.parse(new InputStreamReader(get(url, new HttpClient())));

            Map.Entry value = (Map.Entry)((JSONObject) ((JSONObject) result.get("query")).get("pages")).entrySet().iterator().next();
            String extract = (String)((JSONObject) value.getValue()).get("extract");

            String[] split = extract.split("(===)");

            String target = "";
            for (String s: split)
                if (s.length() > target.length() && (s.trim().startsWith(word)))
                    target = s;

            String [] candidates = target.split("\n");
            for (String candidate : candidates)
            {
                candidate = candidate.trim();
                if (!candidate.isEmpty() && !candidate.startsWith(word))
                {
                    bot.sendMessage(channel, word + ": " + candidate + " " + cont);
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return false;
    }

    private void tryUrbanDictionary(HoggerBot bot, String channel, String word)
    {
        try
        {
            String url = "http://www.urbandictionary.com/define.php?term=" + URLEncoder.encode(word, "UTF-8");
            Document document = Jsoup.parse(new URL(url), 750);
            Elements select = document.select("#not_defined_yet");
            if (select.isEmpty())
            {
                Elements result = document.select("#entries .definition");
                if(!result.isEmpty())
                {
                    Element first = result.first();
                    String html = first.html();
                    if (html.length() < 256)
                    {
                        bot.sendMessage(channel, word + ": " + html);
                    }
                    else
                    {
                        bot.sendMessage(channel, word + ": " + html.substring(0, 180) + "\u2026 " + url);
                    }
                    return;
                }
            }
            bot.sendMessage(channel, "'" + word + "' is not defined yet!");
        }
        catch (UnsupportedEncodingException e) {
            bot.sendMessage(channel, "UTF-8 not found!?");
        } catch (MalformedURLException e) {
            bot.sendMessage(channel, "Cut it out noob!");
        } catch (IOException e) {
            bot.sendMessage(channel, "The Internets are broken");
        }
    }


    private static InputStream get(String url, HttpClient client)
    {
        try
        {
            GetMethod get = new GetMethod(url);
            int status = client.executeMethod(get);
            if (status == HttpStatus.SC_OK)
                return get.getResponseBodyAsStream();
            else
                throw new RuntimeException(get.getStatusText());
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
