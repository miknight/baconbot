package au.com.angry.irc.listeners;

import au.com.angry.irc.HoggerBot;

/**

 */
public class JoinMessageEvent extends MessageEvent
{
    public JoinMessageEvent(HoggerBot bot, String sender, String channel, String message)
    {
        super(bot, sender, channel, message);
    }
}
