package au.com.angry.irc.listeners;

import au.com.angry.irc.HoggerBot;

/**
 */
public class MessageEvent
{
    private final HoggerBot bot;
    private boolean consumed;
    private final String sender;
    private final String channel;
    private final String message;

    public MessageEvent(HoggerBot bot, String sender, String channel, String message)
    {
        this.bot = bot;
        this.sender = sender;
        this.channel = channel;
        this.message = message;
        this.consumed = false;
    }

    public String getChannel()
    {
        return channel;
    }

    public String getSender()
    {
        return sender;
    }

    public String getMessage()
    {
        return message;
    }

    public boolean isConsumed()
    {
        return consumed;
    }

    public void consume()
    {
        consumed = true;
    }

    public void reply(String message)
    {
        if (channel.startsWith("#"))
            bot.sendMessage(channel, sender + ": " + message);
        else
            bot.sendMessage(channel, message);
    }
}
