package au.com.angry.irc.listeners;

import au.com.angry.irc.HoggerBot;

/**
 */
public class ActionEvent extends MessageEvent
{
    public ActionEvent(HoggerBot bot, String sender, String channel, String message)
    {
        super(bot, sender, channel, message);
    }
}
