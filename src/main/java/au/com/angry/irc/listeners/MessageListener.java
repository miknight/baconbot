package au.com.angry.irc.listeners;

import au.com.angry.irc.HoggerBot;

/**
 */
public interface MessageListener
{
    void onMessage(HoggerBot bot, MessageEvent event);
}
