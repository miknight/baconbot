package au.com.angry.irc.listeners;

import au.com.angry.irc.HoggerBot;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
public class SkyrimListener implements MessageListener
{
    private static final String AN="(:?an?)?/s*";
    private static final String vowels = "aeiou";
    private static final Pattern[] PATTERNS = new Pattern[]{
            Pattern.compile("i'?m\\s*(an?\\s+)?\\s*([A-Za-z0-9', ]*).*"),
            Pattern.compile("i am\\s*(an?\\s+)?\\s*([A-Za-z0-9', ]*).*"),
    };

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String message = event.getMessage();
        String m = event.getMessage();
        if(message.contains(":"))
            m = message.substring(message.lastIndexOf(":")+1).trim();
        String what = getWhat(m);
        if(StringUtils.isNotBlank(what)) {
            boolean sayAn = getAnA(message)!=null;
            if(sayAn) {
            boolean an = vowels.indexOf(what.substring(0,1).toLowerCase())!=-1;
                bot.sendOptionalMessage(event.getChannel(), "I used to be a"+(an?"n":"")+" "+what.trim()+" until I took an arrow to the knee.");
            } else {
                bot.sendOptionalMessage(event.getChannel(), "I used to be "+what.trim()+" until I took an arrow to the knee.");
            }
        }
    }

    private static String getWhat(String message) {
        for (Pattern pattern : PATTERNS) {
            Matcher matcher = pattern.matcher(message.toLowerCase());
            if (matcher.matches())
                return matcher.group(2);
        }
        return null;
    }

    private static String getAnA(String message) {
        for (Pattern pattern : PATTERNS) {
            Matcher matcher = pattern.matcher(message.toLowerCase());
            if (matcher.matches())
                return matcher.group(1);
        }
        return null;
    }

}
