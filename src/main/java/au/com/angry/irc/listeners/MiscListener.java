package au.com.angry.irc.listeners;

import au.com.angry.irc.ConnectionSettings;
import au.com.angry.irc.HoggerBot;

public class MiscListener implements MessageListener
{
    private static final String[] LOLS = {"Hahahaha", "lol", "Haha!", "LMAO!", "rofl", "trolololololol"};
    private static final String[] MATCHES = {"lol(ol)*", "(ha)+h?", "trol(ol)+", "rofl", "lmao"};

    private final String nick;

    public MiscListener(ConnectionSettings settings)
    {
        this.nick = settings.getNick();
    }

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String sender = event.getSender();
        String message = event.getMessage();
        String channel = event.getChannel();

        if (this.nick.equals(sender))
            return;

        String lowerCaseMessage = message.toLowerCase();

        if (lowerCaseMessage.contains("thanks, " + nick) || lowerCaseMessage.contains("thanks " + nick) || lowerCaseMessage.contains(nick + ": thanks"))
            bot.sendMessage(channel, ("No problem, " + sender));

        if (lowerCaseMessage.contains("\\o/"))
            bot.sendOptionalMessage(channel, ("\\o/"));

        if (lowerCaseMessage.contains("waddle"))
            bot.sendOptionalMessage(channel, ("Waddle waddle"));

        for (String match : MATCHES)
        {
            if (lowerCaseMessage.matches(match))
            {
                bot.sendOptionalMessage(channel, LOLS[(int) (Math.random() * LOLS.length)]);
                break;
            }
        }

    }
}
