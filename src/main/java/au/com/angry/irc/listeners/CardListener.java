package au.com.angry.irc.listeners;

import au.com.angry.gatherer.MagicCard;
import au.com.angry.irc.Gatherer;
import au.com.angry.irc.HoggerBot;
import org.apache.commons.lang.StringUtils;

/**
 */
public class CardListener implements MessageListener
{

    public static final String PREFIX = "card ";

    @Override
    public void onMessage(HoggerBot bot, MessageEvent event)
    {
        String message = event.getMessage();
        String channel = event.getChannel();
        if (message.startsWith(PREFIX))
        {
            String cardName = message.substring(PREFIX.length());
            try
            {
                MagicCard magicCard = Gatherer.getCard(cardName);
                long multiverseId = magicCard.getPrintings().iterator().next().getMultiverseId();
                bot.sendMessage(channel, "card http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=" + multiverseId);

                StringBuilder buf = new StringBuilder();
                buf.append(magicCard.getName());
                if (StringUtils.isNotBlank(magicCard.getCost()))
                    buf.append(" - ").append(magicCard.getCost());
                buf.append(" - ").append(magicCard.getTypes());
                if (StringUtils.isNotBlank(magicCard.getPt()))
                    buf.append(" - ").append(magicCard.getPt());
                buf.append(" - ").append(magicCard.getText().replaceAll("\n", " / "));
                bot.sendMessage(channel, buf.toString());

            }
            catch (Exception e)
            {
                bot.sendMessage(channel, "card not found");
            }
        }
    }
}
