package au.com.angry.irc;

import org.quartz.*;

import java.io.IOException;
import java.util.Date;

/**
 */
public class ReconnectJob extends IrcJob
{

    public static void register(Scheduler sched, JobDataMap data) throws SchedulerException
    {
        JobDetail jobDetail = new JobDetail("reconnectJob", null, ReconnectJob.class);
        jobDetail.setJobDataMap(data);
        Trigger trigger = TriggerUtils.makeMinutelyTrigger(1);
        trigger.setName("reconnectTrigger");
        sched.scheduleJob(jobDetail, trigger);
    }

    @Override
    protected void execute(HoggerBot bot, Date prev)
    {
        if(!bot.isConnected())
            bot.connect();
        bot.joinChannel();
    }
}

