Coffee is a brewed drink prepared from roasted seeds, commonly called coffee beans, of the coffee plant.
Coffee beans are seeds of coffee cherries that grow on trees in over 70 countries.
Green coffee, for example, is one of the most traded agricultural commodities in the world.
Due to its caffeine content, coffee can have a stimulating effect in humans.
Today, coffee is one of the most popular beverages worldwide.
It is thought that the energizing effect of the coffee bean plant was first recognized in Yemen in Arabia and the north east of Ethiopia, and the cultivation of coffee first expanded in the Arab world.
The earliest credible evidence of coffee drinking appears in the middle of the fifteenth century, in the Sufi monasteries of the Yemen in southern Arabia.
From the Muslim world, coffee spread to Italy, then to the rest of Europe, to Indonesia, and to the Americas.
Coffee has played an important role in many societies throughout history.
In Africa and Yemen, coffee was used in religious ceremonies.
The Ethiopian Church banned its secular consumption of coffee until the reign of Emperor Menelik II of Ethiopia.
Coffee was banned in Ottoman Turkey during the 17th century for political reasons, and was associated with rebellious political activities in Europe.
Coffee berries, which contain the coffee bean, are produced by several species of small evergreen bush of the genus Coffea.
The two most commonly grown are Coffea canephora (also known as Coffea robusta) and Coffea arabica. Both are cultivated primarily in Latin America, Southeast Asia, and Africa.
Once ripe, coffee berries are picked, processed, and dried.
Coffee seeds are roasted to varying degrees, depending on the desired flavor.
Coffee can be prepared and presented in a variety of ways.
An important export commodity, coffee was the top agricultural export for 12 countries in 2004, and in 2005, it was the world's seventh-largest legal agricultural export by value.
Some controversy is associated with coffee cultivation and its impact on the environment.
Many studies have examined the relationship between coffee consumption and certain medical conditions; whether the overall effects of coffee are ultimately positive or negative has been widely disputed.
The first reference to "coffee" in the English language, in the form chaoua, dates to 1598.
In English and other European languages, coffee derives from the Ottoman Turkish kahveh, via the Italian caff�.
Coffee is usually propagated by seeds.
The traditional method of planting coffee is to put 20 seeds in each hole at the beginning of the rainy season; half are eliminated naturally.
An effective method of growing coffee, used in Brazil, is to raise seedlings in nurseries, which are then planted outside at 6 to 12 months.
Coffee is often intercropped with food crops, such as corn, beans, or rice, during the first few years of cultivation.
Of the two main species grown, arabica coffee is considered more suitable for drinking than robusta coffee; robusta tends to be bitter and have less flavor but better body than arabica.
