#!/bin/sh

# chatterbot api
mvn install:install-file -DgroupId=com.google.code \
    -DartifactId=chatterbotapi \
    -Dversion=1.2.1 \
    -Dfile=lib/chatterbotapi-1.2.1.jar \
    -Dpackaging=jar \
    -DgeneratePom=true

mvn --mvn-version 2.1.0 deploy:deploy-file \
    -DgroupId=com.google.code \
    -DartifactId=chatterbotapi \
    -Dversion=1.2.1 \
    -Dfile=chatterbotapi-1.2.1.jar \
    -Dpackaging=jar \
    -DgeneratePom=true \
    -DrepositoryId=angry-release \
    -Durl=scp://optimus.angry.com.au/files/maven/release

